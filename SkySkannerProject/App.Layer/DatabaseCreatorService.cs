﻿using Database.Layer;
using System;
using System.Collections.Generic;
using System.Text;

namespace App.Layer
{
    public class DatabaseCreatorService : IDatabaseCreator
    {
        public void CreateIfDbNotExist()
        {
            using (var context = new SkyScannerContext())
            {
                context.Database.EnsureCreated();
            }
        }
    }
}
