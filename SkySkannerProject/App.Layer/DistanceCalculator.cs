﻿using Database.Layer.Model;
using Geolocation;
using System;

namespace App.Layer
{
    public class DistanceCalculator
    {
        /// <summary>
        /// Method calculates distance from origin point to destination point provided as geo coordinates
        /// </summary>
        /// <param name="originLon">origin point longitude (range: -180 to 180 degrees)</param>
        /// <param name="originLat">origin point latitude (range: -90 to 90 degrees)</param>
        /// <param name="destinationLon">destination point longitude (range: -180 to 180 degrees)</param>
        /// <param name="destinationLat">destination point latitude (range: -90 to 90 degrees)</param>
        /// <returns></returns>
        public double CalculateDistanceFromCoordinates(Airport originAirport, Airport destinationAirport)
        {
            const double milesToKilometersRatio = 1.609;
            var originPoint = new Coordinate(originAirport.Latitude, originAirport.Longitude);
            var destinationPoint = new Coordinate(destinationAirport.Latitude, destinationAirport.Longitude);

            return Math.Round(GeoCalculator.GetDistance(originPoint, destinationPoint)*milesToKilometersRatio, 2);
        }
    }
}