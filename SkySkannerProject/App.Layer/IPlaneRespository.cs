﻿using Database.Layer.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Database.Layer
{
    interface IPlaneRespository
    {
        List<Plane> GetPlanes();
        void AddPlane(Plane plane);
        void DeletePlane(Plane plane);
    }
}
