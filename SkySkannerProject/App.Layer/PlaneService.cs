﻿using Database.Layer;
using Database.Layer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Layer
{
    public class PlaneService
    {
        public void AddPlane(Plane plane)
        {
            using (var _context = new SkyScannerContext())
            {
                _context.Planes.Add(plane);
                _context.SaveChanges();
            }
        }
        public List<Plane> GetPlanes()
        {
            using (var _context = new SkyScannerContext())
            {
                var planes = _context.Planes.ToList();
                return planes;
            }
        }
    }
}
