﻿using Database.Layer;
using Database.Layer.Model;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App.Layer.Service
{
    public class AirportService
    {
        public void AddAirport(Airport airport)
        {
            using (var _context = new SkyScannerContext())
            {                                
                _context.Airports.Add(airport);
                _context.SaveChanges();
            }
        }
        public List<Airport> GetAvailableAirports()
        {
            using (var _context = new SkyScannerContext())
            {
                var airports = _context.Airports.ToList();
                return airports;
            }
        }
        public Airport GetAirportsById(int id)
        {
            using (var _context = new SkyScannerContext())
            {
                var airport = _context.Airports.FirstOrDefault(x => x.AirportId == id);
                return airport;
            }
        }
    }    
}
