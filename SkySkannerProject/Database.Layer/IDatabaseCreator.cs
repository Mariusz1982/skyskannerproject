﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Layer
{
    public interface IDatabaseCreator
    {
        void CreateIfDbNotExist();
    }
}
