﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Layer.Model
{
    public class Airport
    {
        public Airport(string name, double latitude, double longitude)
        {
            Name = name;
            Latitude = latitude;
            Longitude = longitude;
        }
        public int AirportId { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        
        //public List<Connection> Connections { get; set; }
    }
}
