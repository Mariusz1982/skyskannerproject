﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database.Layer.Model
{
    public class Plane
    {
 
        public int Id { get; set; }
        public string Name { get; set; }
        public double AverageSpeed { get; set; }
        public double AverageFuelConsumption { get; set; }
        public int NumberOfSeats { get; set; }
    }
}
