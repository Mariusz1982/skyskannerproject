﻿using Database.Layer.Model;
using Microsoft.EntityFrameworkCore;
using System;

namespace Database.Layer
{
    public class SkyScannerContext : DbContext
    {
        private static string _connectionString = @"Data Source= ..\SkyScannerDB.db";

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite(_connectionString);
            }
        }

        public DbSet<Airport> Airports { get; set; }
        public DbSet<Plane> Planes { get; set; }
    }
}