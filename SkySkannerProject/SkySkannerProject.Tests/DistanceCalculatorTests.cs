using App.Layer;
using Database.Layer.Model;
using System;
using Xunit;

namespace SkySkannerProject.Tests
{
    public class DistanceCalculatorTests
    {
        [Fact]
        public void CalculateDistanceFromCoordinatesValidDataValidResult()
        {
            //Arange
            var originAirport = new Airport()
            {
                AirportId = 1,
                Latitude = 54.379392,
                Longitude = 18.466017,
                Name = "GDA"
            };

            var destinationAirport = new Airport()
            {
                AirportId = 2,
                Latitude = 52.166012,
                Longitude = 20.973699,
                Name = "WAW"
            };

            //Act
            var calculator = new DistanceCalculator();
            var actualValue = calculator.CalculateDistanceFromCoordinates(originAirport, destinationAirport);

            //Assert
            Assert.Equal(297.18d, actualValue);
        }
    }
}