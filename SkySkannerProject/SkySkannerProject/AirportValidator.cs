﻿using Database.Layer.Model;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace SkySkannerProject
{
    public class AirportValidator : AbstractValidator<Airport>
    {
        public AirportValidator()
        {
            RuleFor(airport => airport.Name).NotNull();
            RuleFor(airport => airport.Longitude).NotNull().LessThanOrEqualTo(180).GreaterThanOrEqualTo(-180);
            RuleFor(airport => airport.Latitude).NotNull().LessThanOrEqualTo(90).GreaterThanOrEqualTo(-90);            
        }
    }
}
