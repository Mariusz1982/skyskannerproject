﻿using App.Layer.Service;
using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Database.Layer.Model;
using System.Linq;

namespace SkySkannerProject.Creator
{
    public class AirportCreator
    {
        public Airport CreateAirport()
        {

            Airport airport;
            var validator = new AirportValidator();
            var success = false;
            do
            {
                var airportName = GetAirportName();
                var latitude = GetDouble("szerokość geograficzną");
                var longitude = GetDouble("długość geograficzną");
                airport = new Airport(airportName, latitude, longitude);
                var validationResult = validator.Validate(airport);
                success = validationResult.IsValid;

                if (!success)
                {
                    Console.WriteLine($" {string.Join(" ", validationResult.Errors.Select(x => $"{x.PropertyName}: {x.ErrorMessage}"))}");
                }
            } while (!success);

            return airport;
        }

        public string GetAirportName()
        {
            Console.WriteLine("Podaj nazwę lotniska: ");
            return Console.ReadLine();
        }

        public double GetDouble(string doubleName)
        {
            bool validValue = false;
            double value;            
            do
            {
                Console.WriteLine($"Podaj {doubleName} [WGS84]: ");
                validValue = double.TryParse(Console.ReadLine(), out value);
            } while (!validValue);
            return value;
        }
    }
}
