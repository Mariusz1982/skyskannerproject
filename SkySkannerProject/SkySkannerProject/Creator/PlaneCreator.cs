﻿using System;
using System.Collections.Generic;
using System.Text;
using Database.Layer.Model;
using System.Linq;
using App.Layer;

namespace SkySkannerProject
{
    public class PlaneCreator
    {
        private readonly PlaneValidator _validator;
        private readonly PlaneService _planeService;
        
        public PlaneCreator()
        {
            
             _validator = new PlaneValidator();
            _planeService = new PlaneService();
        }

        public void AddNewPlaneToDataBase()
        {
            Plane plane;
            var validated = false;
            do
            {
                var planeName = GetPlaneName();
                var averageSpeed = GetDouble("average speed");
                var averageFuelConsumption = GetDouble("average fuel consumption");
                var numberOfSeats = GetInt("number of seats");
                plane = new Plane
                {
                    Name = planeName,
                    AverageSpeed = averageSpeed,
                    AverageFuelConsumption = averageFuelConsumption,
                    NumberOfSeats = numberOfSeats,
                };

                var validationResult = _validator.Validate(plane);
                validated = validationResult.IsValid;

                if (!validated)
                {
                    Console.WriteLine($"{string.Join(" ", validationResult.Errors.Select(x => $"{x.PropertyName} : {x.ErrorMessage} "))}");
                }

            } while (!validated);

            _planeService.AddPlane(plane);
        }

        public double GetDouble(string doubleName)
        {
            double value;
            bool validValue;
            do
            {
                Console.WriteLine($"Podaj {doubleName}");
                validValue = double.TryParse(Console.ReadLine(), out value);
            } while (!validValue);
            return value;
        }
        public int GetInt(string intName)
        {
            int value;
            bool validValue;
            do
            {
                Console.WriteLine($"Podaj {intName}");
                validValue = int.TryParse(Console.ReadLine(), out value);
            } while (!validValue);
            return value;
        }

        public string GetPlaneName()
        {
            Console.WriteLine("Insert plane name: ");
            return Console.ReadLine();
        }
    }

}