﻿using App.Layer.Service;
using SkySkannerProject.Creator;
using SkySkannerProject.Menu;
using System;

namespace SkySkannerProject
{
    class MainMenu : MenuService
    {
        private AirportCreator _airportCreator = new AirportCreator();
        private AirportService _airportService = new AirportService();
        private PlaneCreator _planeCreator = new PlaneCreator();
        private MainMenuService _mainMenuService = new MainMenuService();

        public MainMenu()
        {
            AddMenuItem("Add new airport", AddNewAirport);
            AddMenuItem("Add new plane model", AddNewPlaneModel);
            AddMenuItem("for testing: print all airplanes", _mainMenuService.PrintAllPlanes); //for testing
        }

        private void AddNewPlaneModel()
        {
            _planeCreator.AddNewPlaneToDataBase();
        }

        private void AddNewAirport()
        {
            var airport = _airportCreator.CreateAirport();
            _airportService.AddAirport(airport);
        }
    }
}