﻿using System;
using System.Collections.Generic;
using System.Text;
using App.Layer;
using Database.Layer.Model;

namespace SkySkannerProject.Menu
{
    public class MainMenuService
    {
        public void PrintAllPlanes()
        {
            var planeService = new PlaneService();
            var allPlanesList = planeService.GetPlanes();
            foreach(Plane plane in allPlanesList)
            {
                Console.WriteLine($"{plane.Id}. name: {plane.Name}, " +
                    $"average speed: {plane.AverageSpeed} km/h, " +
                    $"average fuel consumption:{plane.AverageFuelConsumption} T/h, " +
                    $"number of seats: {plane.NumberOfSeats}");
            }
        }
    }
}
