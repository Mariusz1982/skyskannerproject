﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SkySkannerProject.Menu
{
    public class MenuItem
    {
        public string Title { get; set; }
        public Action Action { get; set; }
    }
}
