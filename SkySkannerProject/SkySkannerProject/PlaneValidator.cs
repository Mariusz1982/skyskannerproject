﻿using System;
using System.Collections.Generic;
using System.Text;
using Database.Layer.Model;
using FluentValidation;

namespace SkySkannerProject
{
    public class PlaneValidator : AbstractValidator<Plane>
    {
        public PlaneValidator()
        {
            RuleFor(plane => plane.Name).NotNull();
            RuleFor(plane => plane.Id).NotNull().GreaterThanOrEqualTo(0);
            RuleFor(plane => plane.NumberOfSeats).GreaterThanOrEqualTo(0).LessThanOrEqualTo(1000);
            RuleFor(plane => plane.AverageFuelConsumption).NotNull().GreaterThanOrEqualTo(0);
            RuleFor(plane => plane.AverageSpeed).NotNull().GreaterThanOrEqualTo(0);
        }
    }
}
