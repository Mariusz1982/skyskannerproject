﻿using App.Layer;
using System;

namespace SkySkannerProject
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {
            InitializeDatabase();
            InitializeMainMenu();
        }

        private void InitializeMainMenu()
        {
            var mainMenu = new MainMenu();
            mainMenu.Run();
        }

        private void InitializeDatabase()
        {
            var dbService = new DatabaseCreatorService();
            dbService.CreateIfDbNotExist();
        }
    }
}
